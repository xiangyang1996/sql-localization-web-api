This is a simple Web API develop by using .NET CORE 3.1

**Setup Process**
1. Download "Commander/publish" folder from Gitlab
2. Place it under C://
3. Download LocalizationDB.sql from Gitlab
4. Run the sql script in SSMS to import the Database
5. Run "Commander.exe" inside the folder
6. The Web API is running

**How To Use**
1. Download test-tool.html from Gitlab
2. Open it with Goolge Chrome / Firefox
3. Click the Language selection box and select a language
4. Content will be change based on the language selected

Demo Screenshot: https://drive.google.com/file/d/1E7EGpmSCjNWALcAfmNCjWdLlbk5w1_sC/view?usp=sharing

[END]
