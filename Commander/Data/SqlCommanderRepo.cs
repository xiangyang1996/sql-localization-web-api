using System.Collections.Generic;
using System.Linq;
using Commander.Models;

namespace Commander.Data
{
    public class SqlCommanderRepo : ICommanderRepo
    {
        private readonly CommanderContext _context;

        public SqlCommanderRepo(CommanderContext context)
        {
            _context = context;
        }

        public IEnumerable<Command> GetAllCommands()
        {
            return _context.Commands.ToList();
        }
        public IEnumerable<Command> GetLanguageCommands(string culture)
        {
            return _context.Commands.Where(p => p.Culture == culture).ToList();
        }
        public Command GetCommandByKey(string key, string culture)
        {
            return _context.Commands.FirstOrDefault(p => p.Key == key && p.Culture == culture);
        }
    }
}