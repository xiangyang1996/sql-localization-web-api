using System.Collections.Generic;
using Commander.Models;

namespace Commander.Data
{
    public interface ICommanderRepo
    {
        IEnumerable<Command> GetAllCommands();
        IEnumerable<Command> GetLanguageCommands(string culture);
        Command GetCommandByKey(string key, string culture);
    }
}