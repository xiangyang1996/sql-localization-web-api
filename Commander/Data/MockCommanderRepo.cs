using System.Collections.Generic;
using Commander.Models;

namespace Commander.Data
{
    public class MockCommanderRepo : ICommanderRepo
    {
        public IEnumerable<Command> GetAllCommands()
        {
            var commands = new List<Command>
            {
                new Command{Id=0, Key="egg", Text="Boil an egg", Culture="en-US"},
                new Command{Id=1, Key="bread", Text="Get a knife", Culture="en-US"},
                new Command{Id=2, Key="tea", Text="Make cup of tea", Culture="en-US"}
            };

            return commands;
        }

        public IEnumerable<Command> GetLanguageCommands(string culture){
            
            var commands = new List<Command>
            {
                new Command{Id=0, Key="bread", Text="Get a knife", Culture="en-US"},
                new Command{Id=1, Key="tea", Text="Make cup of tea", Culture="en-US"}
            };

            return commands;
        }

        public Command GetCommandByKey(string key, string culture)
        {
            if(culture == "en-US"){
                return new Command{Id=0, Key="egg", Text="Boil an egg", Culture="en-US"};
            }
            else{
                return new Command{Id=0, Key="egg", Text="Masak telur", Culture="ms-MY"};
            }
        }
    }
}