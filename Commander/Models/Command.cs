using System.ComponentModel.DataAnnotations;

namespace Commander.Models
{
    public class Command
    {
        [Key]
        public int Id { get; set;}
        
        [Required]
        public string Key {get; set;}

        [Required]
        [MaxLength(50)]
        public string Culture {get; set;}

        [Required]
        public string Text { get; set; }
    }
}