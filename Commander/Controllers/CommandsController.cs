using System.Collections.Generic;
using Commander.Data;
using Commander.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Commander.Controllers
{
    // api/en-US/commands
    [ApiController]
    public class CommandsController : ControllerBase
    {
        private readonly ICommanderRepo _repository;

        public CommandsController(ICommanderRepo repository)
        {
            _repository = repository;  
        }

        //private readonly MockCommanderRepo _respository = new MockCommanderRepo();
        
        // GET api/{culture}/commands
        [Route("api/[controller]")]
        [HttpGet]
        public ActionResult <IEnumerable<Command>> GetAllCommands()
        {
            var commandItems = _repository.GetAllCommands();

            return Ok(commandItems);
        }

        [Route("api/{culture}/[controller]")]
        [HttpGet]
        public ActionResult <IEnumerable<Command>> GetLanguageCommands()
        {
            string culture = this.ControllerContext.RouteData.Values["culture"].ToString();
            var commandItems = _repository.GetLanguageCommands(culture);

            return Ok(commandItems);
        }

        // GET api/commands/{id}
        [Route("api/{culture}/[controller]/{key}")]
        public ActionResult <Command> GetCommandByKey()
        {
            string culture = this.ControllerContext.RouteData.Values["culture"].ToString();
            string key = this.ControllerContext.RouteData.Values["key"].ToString();
            var commandItem = _repository.GetCommandByKey(key, culture);

            return Ok(commandItem);

        }

    }
}